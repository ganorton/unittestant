import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class HelloTest {
	@Test
    public void sumTest() {
        int a = 1;
        int b = 1;
        int sum = a + b;
        assertEquals(2, sum);
    }
    
    @Test
    public void productTest() {
        int a = 3;
        int b = 5;
        int product = a * b;
        assertEquals(15, product);
    }
    
    @Test
    public void failTest() {
        int a = 1;
        int b = 1;
        int sum = a + b;
        assertEquals(3, sum);
    }
}

